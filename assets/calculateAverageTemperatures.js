function dateEquals(d, o) {
    return ((d && d.getFullYear && o && o.year && d.getFullYear() === o.year) &&
            (d.getMonth && o.month && d.getMonth() === o.month - 1) &&
            (d.getDate && o.date && d.getDate() === o.date));
}

"[" + data
    .sort(function(a, b) {
        return (a && a.timestamp && b && b.timestamp) ? ((a.timestamp>b.timestamp) ? 1 : ((b.timestamp>a.timestamp) ? -1 : 0)) : 0;
    })
    .reduce(function(a, b) {
        var d = new Date(b.timestamp || 0);
        (a && a.length>0 && dateEquals(d, a[a.length-1]) && a[a.length-1].temperatures.push(b.temperature)) ||
        (a.push({year: d.getFullYear(), month: d.getMonth()+1, date: d.getDate(), temperatures: [b.temperature || 0]}));            
        return a;
    }, [])
    .reduce(function(a, b) {
        var s = b.temperatures.reduce(function(c, d) { return c+d }, 0);
        var m = b.temperatures.length === 0 ? 0 : (s/b.temperatures.length).toFixed(3);
        var d = b.year + "-" + (b.month<10 ? "0"+b.month : b.month) + "-" + (b.date<10 ? "0"+b.date : b.date);
        a.push('{"date":"' + d + '","averageTemperature":' + m + ',"entity":"celsius"}'); 
        return a;
    }, [])
    .join(",") + "]";

