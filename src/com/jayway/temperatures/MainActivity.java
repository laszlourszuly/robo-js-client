package com.jayway.temperatures;

import java.util.Calendar;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.DatePicker;

import com.google.gson.Gson;
import com.jayway.temperatures.ContentCollector.OnContentListener;
import com.jayway.temperatures.JavaScriptRunner.OnScriptEngineListener;

public class MainActivity extends ListActivity implements OnContentListener, OnScriptEngineListener {

    private static final String URL = "http://malmo-temp.herokuapp.com/temp";

    private static final int DIALOG_PICKSTARTDATE = 1;
    private static final int DIALOG_PICKENDDATE = 2;

    private Button buttonFetch;
    private ProgressDialog spinner;

    private int startYear;
    private int startMonth;
    private int startDay;
    private int endYear;
    private int endMonth;
    private int endDay;

    private void fetchData() {
        String startDate = getDateString(startYear, startMonth, startDay);
        String endDate = getDateString(endYear, endMonth, endDay);
        String url = URL + "?from=" + startDate + "&to=" + endDate;
        (new ContentCollector(this, this)).execute(url);
    }

    private String getDateString(int year, int month, int date) {
        final StringBuilder builder = new StringBuilder();

        builder.append(year);
        builder.append("-");

        if (month < 10) {
            builder.append(0);
        }

        builder.append(month);
        builder.append("-");

        if (date < 10) {
            builder.append(0);
        }

        builder.append(date);

        return builder.toString();
    }

    @Override
    public void onContentReceive(String json) {
        (new JavaScriptRunner(this, this)).execute(json);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Calendar calendar = Calendar.getInstance();
        calendar.set(1990, Calendar.JANUARY, 1);
        setStartDate(calendar);

        calendar.roll(Calendar.DAY_OF_YEAR, 10);
        setEndDate(calendar);

        buttonFetch = (Button) findViewById(R.id.activity_main_button_fetch);
        buttonFetch.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonFetch.setEnabled(false);
                spinner = ProgressDialog.show(MainActivity.this, MainActivity.this.getString(R.string.app_name), "Working...");
                fetchData();
            }
        });

        setFetchButtonText();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.activity_main_menu_setstartdate:
            showDialog(DIALOG_PICKSTARTDATE);
            break;
        case R.id.activity_main_menu_setenddate:
            showDialog(DIALOG_PICKENDDATE);
            break;
        default:
            break;
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DIALOG_PICKSTARTDATE: {
            DatePickerDialog dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int month, int date) {
                    final Calendar calendar = Calendar.getInstance();
                    calendar.set(year, month, date);
                    setStartDate(calendar);
                    calendar.roll(Calendar.DAY_OF_YEAR, 10);
                    setEndDate(calendar);
                    setFetchButtonText();
                }
            }, startYear, startMonth - 1, startDay);
            dialog.setTitle(R.string.activity_main_label_setstartdate);
            return dialog;
        }
        case DIALOG_PICKENDDATE: {
            DatePickerDialog dialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                public void onDateSet(DatePicker view, int year, int month, int date) {
                    final Calendar calendar = Calendar.getInstance();
                    calendar.set(year, month, date);
                    setEndDate(calendar);
                    setFetchButtonText();
                }
            }, endYear, endMonth - 1, endDay);
            dialog.setTitle(R.string.activity_main_label_setenddate);
            return dialog;
        }
        default:
            return null;
        }
    }

    @Override
    public void onScriptExecuted(String resultJson) {
        Log.d("MYTAG", resultJson);
        Gson gson = new Gson();
        Temperature[] temperatures;
        try {
            temperatures = gson.fromJson(resultJson, Temperature[].class);
        } catch (Exception e) {
            temperatures = null;
        }
        setListAdapter(new TemperatureAdapter(this, temperatures));
        spinner.dismiss();
        buttonFetch.setEnabled(true);
    }

    private void setEndDate(Calendar calendar) {
        if (calendar == null) {
            calendar = Calendar.getInstance();
        }

        endYear = calendar.get(Calendar.YEAR);
        endMonth = calendar.get(Calendar.MONTH) + 1;
        endDay = calendar.get(Calendar.DATE);
    }

    private void setFetchButtonText() {
        if (buttonFetch != null) {
            String startDateString = getDateString(startYear, startMonth, startDay);
            String endDateString = getDateString(endYear, endMonth, endDay);
            String text = getString(R.string.activity_main_label_calculate) + "\n" + startDateString + " to " + endDateString;
            buttonFetch.setText(text);
        }
    }

    private void setStartDate(Calendar calendar) {
        if (calendar == null) {
            calendar = Calendar.getInstance();
        }

        startYear = calendar.get(Calendar.YEAR);
        startMonth = calendar.get(Calendar.MONTH) + 1;
        startDay = calendar.get(Calendar.DATE);
    }
}
