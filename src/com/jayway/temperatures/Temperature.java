package com.jayway.temperatures;

import com.google.gson.annotations.SerializedName;

final class Temperature {
    @SerializedName("date")
    public String date;

    @SerializedName("averageTemperature")
    public float temperature;

    @SerializedName("entity")
    public String scale;
}