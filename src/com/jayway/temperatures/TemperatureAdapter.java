package com.jayway.temperatures;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class TemperatureAdapter extends BaseAdapter {

    private static final class ViewHolder {
        private final TextView date;
        private final TextView temperature;
        private final TextView scale;

        private ViewHolder(TextView date, TextView temperature, TextView scale) {
            this.date = date;
            this.temperature = temperature;
            this.scale = scale;
        }
    }

    private final Temperature[] data;
    private final LayoutInflater inflater;

    public TemperatureAdapter(Context context, Temperature[] data) {
        this.data = data;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return data == null ? 0 : data.length;
    }

    @Override
    public Object getItem(int position) {
        return position >= 0 && position < getCount() ? data[position] : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView == null ? newView() : convertView;
        bindView(position, view);
        return view;
    }

    private View newView() {
        View view = inflater.inflate(R.layout.listitem_temperature, null, false);
        view.setTag(new ViewHolder(
                (TextView) view.findViewById(R.id.listitem_temperature_label_date),
                (TextView) view.findViewById(R.id.listitem_temperature_label_temperature),
                (TextView) view.findViewById(R.id.listitem_temperature_label_scale)));
        return view;
    }

    private void bindView(int position, View view) {
        ViewHolder holder = (ViewHolder) view.getTag();
        Temperature temperature = (Temperature) getItem(position);
        if (holder != null && temperature != null) {
            holder.date.setText(temperature.date);
            holder.temperature.setText(Float.toString(temperature.temperature));
            holder.scale.setText("celsius".equals(temperature.scale) ? "C" : "");
        }
    }
}
