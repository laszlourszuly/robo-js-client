package com.jayway.temperatures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import com.google.gson.JsonSyntaxException;

public final class ContentCollector extends AsyncTask<String, Void, String> {

    private Context context;
    private OnContentListener listener;

    public interface OnContentListener {
        public void onContentReceive(String json);
    }

    public ContentCollector(Context context, OnContentListener listener) {
        if (context == null) {
            throw new IllegalArgumentException("Context mustn't be null");
        }

        this.context = context;
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... params) {
        String json = null;
        boolean hasNetwork = isNetworkAvailable();

        if (hasNetwork && params != null && params.length > 0) {
            HttpURLConnection connection = null;
            BufferedReader reader = null;

            try {
                final URL url = new URL(params[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.addRequestProperty("Cache-Control", "no-cache");

                final StringBuilder builder = new StringBuilder();
                reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String line;

                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }
                json = builder.toString();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            } finally {
                try {
                    if (reader != null) {
                        reader.close();
                    }
                    if (connection != null) {
                        connection.disconnect();
                    }
                } catch (IOException e) {
                }
            }
        }

        return json;
    }

    private boolean isNetworkAvailable() {
        // Check if the mobile device is online.
        // NOTE! This one requires the ACCESS_NETWORK_STATE permission.
        final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return (networkInfo != null) && (networkInfo.isConnected());
    }

    protected void onPostExecute(String result) {
        if (listener != null) {
            listener.onContentReceive(result);
        }
    }
}
