package com.jayway.temperatures;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.content.Context;
import android.os.AsyncTask;

import com.echsylon.robojs.V8;

public final class JavaScriptRunner extends AsyncTask<String, Void, String> {

    private Context context;
    private OnScriptEngineListener listener;

    public interface OnScriptEngineListener {
        public void onScriptExecuted(String resultJson);
    }

    public JavaScriptRunner(Context context, OnScriptEngineListener listener) {
        if (context == null) {
            throw new IllegalArgumentException("Context mustn't be null");
        }

        this.context = context;
        this.listener = listener;
    }

    @Override
    protected String doInBackground(String... data) {
        String result = null;
        BufferedReader reader = null;

        try {
            InputStream input = context.getAssets().open("calculateAverageTemperatures.js");
            reader = new BufferedReader(new InputStreamReader(input, "UTF-8"));

            StringBuilder builder = new StringBuilder();
            String line;

            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }

            final V8 roboJs = new V8();
            roboJs.initialize();
            roboJs.executeJavaScript("var data = " + data[0] + ";");
            result = roboJs.executeJavaScript(builder.toString());
            roboJs.dispose();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        if (listener != null) {
            listener.onScriptExecuted(result);
        }
    }
}
